'use strict';

class UIHandler {
  constructor() {
    // Create table element
    this.UIElement = document.createElement('table')

    // Create header
    const header = this.UIElement.createTHead()
    const row = header.insertRow(0)
    row.insertCell(-1).outerHTML = '<th>Firstname</th>'
    row.insertCell(-1).outerHTML = '<th>Lastname</th>'
    row.insertCell(-1).outerHTML = '<th>Address</th>'
    row.insertCell(-1).outerHTML = '<th>Phone</th>'

    // Create body
    this.UIElement.createTBody()

    // References to callbacks
    this.deleteCallback = () => {}
  }

  addMember(member) {
    // Create row
    const row = this.UIElement.tBodies[0].insertRow(-1) // get first body and insert row

    // Add member to table
    row.id = member.id
    // Hard way:
    // row.insertCell(-1).appendChild(document.createTextNode(member.firstname)) 
    // Easy way:
    row.insertCell(-1).textContent = member.firstname
    row.insertCell(-1).textContent = member.lastname
    row.insertCell(-1).textContent = member.address
    row.insertCell(-1).textContent = member.phone

    // Add buttons
    const deleteBtn = document.createElement('button')
    deleteBtn.innerHTML = 'Delete'
    deleteBtn.id = member.id
    // deleteBtn.onclick = deleteBtn => { this.deleteCallback(deleteBtn.id) }
    deleteBtn.onclick = this.deleteCallback(deleteBtn.id)
    row.insertCell(-1).appendChild(deleteBtn)

    const editBtn = document.createElement('button')
    editBtn.innerHTML = 'Edit'
    // deleteBtn.id = 'btnEdit'
    // editBtn.onclick = this.editCallback
    row.insertCell(-1).appendChild(editBtn)
  }

  getMember(id) {
    // Find element with id, and return
    const memberHTML = document.getElementById(id)
    return {
      id: id,
      firstname: memberHTML.children[0].innerHTML,
      lastname: memberHTML.children[1].innerHTML,
      address: memberHTML.children[2].innerHTML,
      phone: memberHTML.children[3].innerHTML,
    }
  }

  deleteMember(id) {
    // Delete row with given id
    this.UIElement.deleteRow(id)
  }

  editMember(member) {

  }

  set editMemberCallback(cb) {

  }

  set deleteMemberCallback(cb) {
    // For all future buttons
    this.deleteCallback = (btn) => {
      return cb(btn.id)
    }
    let i = ''

    // For all existing buttons in document
    new Array(...document.getElementsByTagName('button'))
      .forEach(btn => {
        // btn.onclick = (el) => { 
        //   return cb(el.id) 
        // }(btn)



        // btn.addEventListener('click', btn => {
        //   cb(btn.id)
        //   console.log(btn.id)
        // })
    })
  }

  // get deleteMemberCallback() {
  //   return ()
  // }

  get length() {
    return this.UIElement.rows.length - 1
  }
}