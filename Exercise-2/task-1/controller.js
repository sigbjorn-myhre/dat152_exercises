class AppController {
    constructor(memberlist,addMember) {
      this.memberlist = memberlist
      this.addMember = addMember
    }

    run() {
      this.ui = new UIHandler()
      document.getElementById(this.memberlist).appendChild(this.ui.UIElement)

      this.ui.deleteMemberCallback = (id) => {
        const doDelete = window.confirm(`Do you really want to delete member with id ${id}?`)
        if (doDelete) {
          window.alert(`Member ${id} should be deleted.`)
        } else  {
          window.alert(`Member ${id} should not be deleted.`)
        }
      }

      this.ui.addMember({"id":2,"firstname":"Per","lastname":"Persen","address":"Persenbakken 77","phone":"14546567"})
      this.ui.addMember({"id":1,"firstname":"Ole","lastname":"Olsen","address":"Olsenbakken","phone":"91826453"})
      //window.setTimeout(() => {this.ui.deleteMember(2)}, 5000)
      // const member = this.ui.getMember(2)
      // window.alert(`firstname:${member.firstname}\nlastname:${member.lastname}\naddress:${member.address}\nphone:${member.phone}`)
      //window.alert(`Number of members are ${this.ui.length}.`)
    }
}

const app = new AppController('memberlist', 'addMember')
document.addEventListener('DOMContentLoaded',app.run.bind(app),true)
